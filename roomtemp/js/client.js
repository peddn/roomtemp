import Test from './Test.js';

let test = new Test();
test.helloWorld();

document.addEventListener('DOMContentLoaded', function() {
    let modal = document.getElementById('flash-modal');
    if(modal) {
        let modalInstance = M.Modal.init(modal);
        modalInstance.open();
    }

    let dropdowns = document.querySelectorAll('.dropdown-trigger');
    let dropdownInstances = M.Dropdown.init(dropdowns, {
        constrainWidth: false,
        coverTrigger: false
    });

});




