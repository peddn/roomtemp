import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, json
)
from werkzeug.security import check_password_hash, generate_password_hash

from roomtemp.db import get_db

bp = Blueprint('reading', __name__, url_prefix='/reading')

@bp.route('/create', methods = ['POST'])
def create():
    print (request.is_json)
    content = request.get_json()
    deviceId = content['deviceid']
    temperature = content['temperature']
    

    db = get_db()
    db.execute(
        'INSERT INTO reading (device_id, temperature)'
        ' VALUES (?, ?)',
        (deviceId, temperature)
    )
    db.commit()

    return 'JSON posted'

@bp.route('/read', methods = ['GET'])
def read():
    db = get_db()
    readings = db.execute(
        'SELECT r.id, device_id, created, temperature'
        ' FROM reading r'
        ' ORDER BY created'
    ).fetchall()
    return json.dumps([dict(r) for r in readings])