from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from roomtemp.auth import login_required
from roomtemp.db import get_db

bp = Blueprint('device', __name__)

@bp.route('/')
def index():
    db = get_db()
    devices = db.execute(
        'SELECT d.id, device_id, location, user_id, username'
        ' FROM device d JOIN user u ON d.user_id = u.id'
        ' ORDER BY location'
    ).fetchall()
    return render_template('device/index.html', devices=devices)

@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        deviceId = request.form['deviceid']
        location = request.form['location']
        error = None

        if not deviceId:
            error = 'device id is required.'
        elif not location:
            error = 'location is required'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO device (device_id, location, user_id)'
                ' VALUES (?, ?, ?)',
                (deviceId, location, g.user['id'])
            )
            db.commit()
            return redirect(url_for('device.index'))

    return render_template('device/create.html')

def get_device(id, check_user=True):
    device = get_db().execute(
        'SELECT d.id, device_id, location, user_id, username'
        ' FROM device d JOIN user u ON d.user_id = u.id'
        ' WHERE d.id = ?',
        (id,)
    ).fetchone()

    if device is None:
        abort(404, "device id {0} doesn't exist.".format(id))

    if check_user and device['user_id'] != g.user['id']:
        abort(403)

    return device

@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    device = get_device(id)

    if request.method == 'POST':
        deviceId = request.form['deviceid']
        location = request.form['location']
        error = None

        if not deviceId:
            error = 'device id is required.'
        elif not location:
            error = 'location is required'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE device SET device_id = ?, location = ?'
                ' WHERE id = ?',
                (deviceId, location, id)
            )
            db.commit()
            return redirect(url_for('device.index'))

    return render_template('device/update.html', device=device)

@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_device(id)
    db = get_db()
    db.execute('DELETE FROM device WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('device.index'))

@bp.route('/<int:id>/show')
def show(id):
    device = get_device(id)
    return render_template('device/show.html', device=device)