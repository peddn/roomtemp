const fs = require('fs-extra');

console.log('Building client...');

try {
    fs.copySync('node_modules/materialize-css/sass', 'roomtemp/scss/materialize-css');
    console.log('materilalize-css scss directory copied.');
    fs.copySync('node_modules/materialize-css/dist/js/materialize.min.js', 'roomtemp/static/js/materialize.min.js');
    console.log('materilalize-css js copied.');
    fs.copySync('node_modules/jquery/dist/jquery.min.js', 'roomtemp/static/js/jquery.min.js');
    console.log('jquery copied.');
} catch (err) {
    console.error(err);
  }