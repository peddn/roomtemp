const path = require('path');

module.exports = {
    mode: 'development',
    entry: path.resolve(__dirname, 'roomtemp', 'js', 'client.js'),
    output: {
        filename: 'client.js',
        path: path.resolve(__dirname, 'roomtemp', 'static', 'js')
    }
};